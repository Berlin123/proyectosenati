<?php

class controller{


    //constructor cargara y levantara todo lo que este construido 
    //dentro si necesidad de inicializar nada 
    function __construct(){

        //echo "Controlador Base";
        
        //variable declara en la clase controller y que si se herda en otra 
        //clase se podra usar
        $this->view = new View(); 

    }


    function CargarModel($model){
      $url = 'models/'.$model.'model.php';

      if(file_exists($url)){

         require $url;
          //no es lo mismo model.php que model creo que es por que detecta automatico el php puede que se 
          //una funcion de el mismo
         $modelNombre = $model.'Model';
         //declaramos la variable en el contructor para que cuando sea heredada se puede elegir en otras clases
         $this->model = new $modelNombre();

      }


    }



}




?>