<?php

//Son las plantillas bases para inicializar nuestra app
class Metodos{

    //constructor todo lo que se cree aqui se ejecutara automaticamente
     function __construct(){

       // echo "Hola mundo<br>";
       //preguntaremos a la variable url si es que no encuentra que por defecto le coloque null
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = explode('/', $url);
        
        //validaremos si es que la url esta vacia retornar al controlador main
        if(empty($url[0])){

        //lo primero que se vera aqui hice cambio y lo remplaze por principal antes era main
            $rutacontrolladores = 'Controllers/main.php';
            require $rutacontrolladores;
            $controllador = new Main();
            //aqui lo mismo era main pero ahora principal son pruebas por ahora
            $controllador->CargarModel('main');
            $controllador->ReturView();

            return false;
            //para cerrar si es cierto 

        }

        $rutacontrolladores = 'Controllers/'.$url[0].'.php';
       
        //echo $url;

        //Si el archivo existe y esta en la carpeta Controllers
        // te llevara al controlador de indicado que escribas en la url
        if(file_exists($rutacontrolladores)){
            
            require_once $rutacontrolladores;
            $controllador = new $url[0];
            //llamando al metodo del controllador model
            $controllador->CargarModel($url[0]);


              ///para invocar a los metodos de nuestros controlladores
            if(isset($url[1])){

                $controllador->{$url[1]}();

            }//si no existe el metodo cargar la funcion retorvista que lo llevara al index
            else{
                $controllador->ReturView();
            }

        }
        //Si no te llevara al controlador de de error
        else
        {
 

            require_once  'Controllers/error.php';
            $controllador = new Errores();
        }
       
        



     }

}

?>