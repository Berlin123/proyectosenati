<?php

class Database{

    //declarando variables LOCALES

private $host;
private $db;
private $user;
private $password;
private $charset;
var $pdo;



    //creando constructor 

    public function __construct(){
       //SETEANDO LOS DATOS DE LAS VARIABLES GLOBALES A LAS VARIABLES LOCALES
        $this->host = constant('HOST');
        $this->db = constant('DB');
        $this->user = constant('USER');
        $this->password = constant('PASSWORD');
        $this->charset = constant('CHARSET');
    }


     function Conexion(){

       try{
            $connection = "mysql:host=". $this->host .";dbname=". $this->db ;
            //para poder tener descripccion de los errores
            $options = [ PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_EMULATE_PREPARES =>false,];

            $pdo = new PDO($connection,$this->user,$this->password,$options);
            

            return $pdo;
       }catch(PDOException $e){

           print_r('Error Conexion'.$e->getMessage());

       }



    }




}



?>