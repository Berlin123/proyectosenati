<?php

class Consulta extends controller{

    function __construct(){
        //ya herredador la clase controlador y sus metodos llamamos asu metodo ReturView y 
        //le daremos la direccion del archivo que mostraremos
        parent::__construct();

        //mensaje que mandaremos a la vista que elijamos
        $this->view->mensaje = "Vista de Consulta";
        //declaro la variable datos tipo arreglo
        $this->view->datos = [];

        }


        function ReturView(){

            //recojiendo los datos del modelo
            //llamamo al metodo consulta de mi capa modelo/Consultamodel
            $recojiendodatos = $this->model->Consultar();
            $this->view->datos = $recojiendodatos;
            $this->view->ReturView('ViewConsulta/index.php');
        }




}


?>