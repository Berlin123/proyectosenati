<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php require 'Views/header.php' ?>    

<?php 

echo $this->mensaje;

?>


<!-- llamando la variable que declare en mi constructor que ahora contiene 
datos ya que esta usando el metodo Consultardemodel-->


<table>
<thead>
<tr>
<th>Nombre</th>
<th>Apellido</th>
<th>Edad</th>
<th>Nacionalidad</th>
</tr>
</thead>

<tbody>


<?php
//llamamos a nuestra entidad
include_once 'models/datos.php';
//aca decimos que nuestros variable datos que esta almacenando a un conjunto de datos de el tipo de entidad de datos.php
//lo sepraremos uno por uno y declaramos una variable del mismo tipo de entidad para alamacenar en su correspondiente atributo
foreach( $this->datos as $campos){
    $datos = new Datos();
    $datos = $campos;
    ?>    
    <tr>
        <th><?php echo $datos->Nombre; ?></th>
        <th><?php echo $datos->Apellido; ?></th>
        <th><?php echo $datos->Edad; ?></th>
        <th><?php echo $datos->Nacionalidad; ?></th>
    </tr>
<?php } ?>

</tbody>
</table>


<?php require 'Views/footer.php' ?>    

</body>
</html>